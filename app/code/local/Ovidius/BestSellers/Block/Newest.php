<?php
class Ovidius_Bestsellers_Block_Newest extends Mage_Catalog_Block_Product_Abstract
{
    public function getCollection()
    {
        $limit = $this->getLimit();
        $limit = isset($limit) ? $limit : 6;
        $category = Mage::registry('current_category');
        $categoryId = $category->getId();
        $storeId = Mage::app()->getStore()->getId();

        $childCategories = $category->getChildren();
        $categories = array();

        if ($childCategories != "")
            $categories = explode(',', $childCategories);   // include child categories

        array_push($categories, $categoryId);   // append parent category ID

        // Filter products added in last 14 days
        $time = time();
        $to = date('Y-m-d H:i:s', $time);
        $lastTime = $time - 1209600; // 60*60*24*14
        $from = date('Y-m-d H:i:s', $lastTime);

        $collection = Mage::getModel('catalog/product')->getCollection()
                    ->addAttributeToSelect(array('name', 'price', 'small_image'))
                    ->joinField('category_id', 'catalog/category_product', 'category_id', 'product_id = entity_id', null, 'left')
                    ->addFieldToFilter('category_id', $categories)
					->addStoreFilter($storeId)
                    ->addFieldToFilter('created_at', array('from' => $from, 'to' => $to))
                    ->setOrder('created_at', 'desc');

        // filter only in stock products
        Mage::getSingleton('cataloginventory/stock')->addInStockFilterToCollection($collection);
        $collection;
        $collection->setPage(1, $limit);
        return $collection;
    }

    public function getHeader()
    {
        return $this->__('Newest products');
    }

    public function getId()
    {
        return 'bestsellers_newest';
    }
}
