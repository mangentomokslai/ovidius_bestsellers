<?php
class Ovidius_Bestsellers_Block_Bestsellers extends Mage_Catalog_Block_Product_Abstract
{
    public function getCollection()
    {
        $limit = $this->getLimit();
        $limit = isset($limit) ? $limit : 6;
        $category = Mage::registry('current_category');
        $categoryId = $category->getId();
        $storeId = Mage::app()->getStore()->getId();

        $childCategories = $category->getChildren();
        $categories = array();

        if ($childCategories != "")
            $categories = explode(',', $childCategories);   // include child categories

        array_push($categories, $categoryId);   // append parent category ID

        $collection = Mage::getResourceModel('reports/product_collection')
            ->addOrderedQty()
            ->joinField('category_id', 'catalog/category_product', 'category_id', 'product_id = entity_id')
            ->addFieldToFilter('category_id', $categories)
            ->setStoreId($storeId)
            ->addStoreFilter($storeId)
            ->setOrder('ordered_qty', 'desc')
            ->addAttributeToSelect(array('name', 'price', 'small_image'));
        
        // filter only in stock products
        Mage::getSingleton('cataloginventory/stock')->addInStockFilterToCollection($collection);
        $collection->setPage(1, $limit);
        return $collection;
    }

    public function getHeader()
    {
        return $this->__('Bestsellers');
    }

    public function getId()
    {
        return 'bestsellers';
    }
}
